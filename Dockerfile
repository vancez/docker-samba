FROM alpine:3.6

RUN apk --no-cache add samba \
    && mkdir /data \
    && chmod 777 /data

VOLUME ["/data"]

COPY smb.conf /etc/samba/smb.conf

EXPOSE 445

CMD ["smbd", "-FS"]
