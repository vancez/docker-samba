# Samba

## Default configuration
anonymous and read only samba server
```
[global]
    security = user
    map to guest = Bad User

[shared]
    path = /data
    browseable = yes
    read only = yes
    guest ok = yes
```
